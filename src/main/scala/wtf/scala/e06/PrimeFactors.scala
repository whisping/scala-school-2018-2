package wtf.scala.e06

object PrimeFactors {

  /**
    * Find all prime factors of positive number and return them in ascending order
    * @param num
    * @return
    */
  def primeFactors(num: Int): Seq[Int] = ???

  /**
    * Find all prime factors of positive number and return them and their multiplicities
    * as Map of factor -> multiplicity
    * @param num
    * @return
    */
  def primeFactorsMultiplicity(num: Int): Map[Int, Int] = ???

}
